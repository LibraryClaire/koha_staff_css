
 <div id="menu">
 <ul>
 [% IF ( accounting ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Accounting" href="/cgi-bin/koha/admin/preferences.pl?tab=accounting"><i class="fa fa-money"></i> Accounting</a></li>
 [% IF ( acquisitions ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Acquisitions" href="/cgi-bin/koha/admin/preferences.pl?tab=acquisitions"><i class="fa fa-gift"></i> Acquisitions</a></li>
 [% IF ( admin ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Administration" href="/cgi-bin/koha/admin/preferences.pl?tab=admin"><i class="fa fa-gear"></i> Administration</a></li>
 [% IF ( authorities ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Authority Control" href="/cgi-bin/koha/admin/preferences.pl?tab=authorities"><i class="fa fa-link"></i> Authorities</a></li>
 [% IF ( cataloguing ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Cataloging" href="/cgi-bin/koha/admin/preferences.pl?tab=cataloguing"><i class="fa fa-tag"></i> Cataloging</a></li>
 [% IF ( circulation ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Circulation" href="/cgi-bin/koha/admin/preferences.pl?tab=circulation"><i class="fa fa-exchange"></i> Circulation</a></li>
 [% IF ( enhanced_content ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Enhanced content settings" href="/cgi-bin/koha/admin/preferences.pl?tab=enhanced_content"><i class="fa fa-plus"></i> En    hanced content</a></li>
[% IF ( i18n_l10n ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Internationalization and localization" href="/cgi-bin/koha/admin/preferences.pl?tab=i18n_l10n"><i class="fa fa-flag-o"></i> I1    8N/L10N</a></li>
[% IF ( local_use ) %]<li class="active">[% ELSE %]<li>[% END %]<a href="/cgi-bin/koha/admin/systempreferences.pl"><i class="fa fa-file-code-o"></i> Local use</a></li>
[% IF ( logs ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Transaction logs" href="/cgi-bin/koha/admin/preferences.pl?tab=logs"><i class="fa fa-file-archive-o"></i> Logs</a></li>
[% IF ( opac ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Online Public Access Catalog" href="/cgi-bin/koha/admin/preferences.pl?tab=opac"><i class="fa fa-globe"></i> OPAC</a></li>
[% IF ( patrons ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Patrons" href="/cgi-bin/koha/admin/preferences.pl?tab=patrons"><i class="fa fa-address-card-o"></i> Patrons</a></li>
[% IF ( searching ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Searching" href="/cgi-bin/koha/admin/preferences.pl?tab=searching"><i class="fa fa-search-plus"></i> Searching</a></li>
[% IF ( serials ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Serials" href="/cgi-bin/koha/admin/preferences.pl?tab=serials"><i class="fa fa-newspaper-o"></i> Serials</a></li>
[% IF ( staff_client ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Staff client" href="/cgi-bin/koha/admin/preferences.pl?tab=staff_client"><i class="fa fa-dashboard"></i> Staff client</a></    li>
[% IF ( tools ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Tools" href="/cgi-bin/koha/admin/preferences.pl?tab=tools"><i class="fa fa-gear"></i> Tools</a></li>
[% IF ( web_services ) %]<li class="active">[% ELSE %]<li>[% END %]<a title="Web services" href="/cgi-bin/koha/admin/preferences.pl?tab=web_services"><i class="fa fa-internet-explorer"></i> Web servic    es</a></li>
</ul>
</div>